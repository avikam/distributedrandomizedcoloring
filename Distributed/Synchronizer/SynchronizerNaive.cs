﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distributed
{
    class SynchronizerNaive : ISynchronizer
    {
        public uint Cycle { get; private set; }

        public bool Terminated {get; private set; }

        public event FinishedEventHandler Finished;

        private IList<Vertex> vertices;
        private int totalTerminated;
        private Barrier barrier;
        private object obj;

        public SynchronizerNaive(IList<Vertex> vertices)
        {
            obj = new object();
            this.vertices = vertices;
        }

        public void Start()
        {
            Cycle = 0;
            Terminated = false;
            barrier = new Barrier(vertices.Count, (b) => Cycle++);

            IEnumerable<Action> actions = vertices.Select<Vertex, Action>(v => v.Run);
            Parallel.Invoke(actions.ToArray());
        }

        // TODO: Consider changing this signature so that vertices can't cheat (signal end on behald of others)
        public void EndRun()
        {
            barrier.SignalAndWait();
        }

        public void Terminate(Vertex v)
        {
            lock (obj)
            {
                Interlocked.Increment(ref totalTerminated);
                if (totalTerminated == vertices.Count)
                {
                    Terminated = true;
                    Console.WriteLine("All vertexes done, within {0} cycles", Cycle + 1);

                    if (Finished != null)
                    {
                        Finished(this, new EventArgs());
                    }
                }
                else if (totalTerminated > vertices.Count)
                {
                    throw new Exception("Algorithm is done!");
                }
            }
        }
    }
}
