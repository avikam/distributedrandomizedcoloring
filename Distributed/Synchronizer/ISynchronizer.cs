﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distributed
{
    public delegate void FinishedEventHandler(object sender, EventArgs args);

    public interface ISynchronizer
    {
        // start synchronizer
        void Start();

        uint Cycle { get; }


        void EndRun();
        
        void Terminate(Vertex v);

        bool Terminated { get; }

        event FinishedEventHandler Finished;
    }
}
