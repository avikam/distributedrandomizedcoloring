﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Distributed.StaticGraph;
using CommandLine;
using CommandLine.Text;

namespace Distributed
{
    class Options
    {
        [Option('a', "algorithm", Required = true, DefaultValue = "Rand2Delta", HelpText = @"[Rand2Delta[,RandDeltaPlus1[,Rand2Delta2[,FastRand[,FastRandDeltaPlus1[,FastRand2[,ColeVishkin]]]]]]")]
        public string Algorithm { get; set; }

        [Option('i', "input", Required = true, DefaultValue = "", HelpText = "Input graph file")]
        public string InputFile {get; set;}

        [Option('r', "ro", Required = false, DefaultValue = 1, HelpText = "FastRand Ro. Default, Ro=1")]
        public int Ro { get; set; }

        [Option('c', "count", Required = false, DefaultValue = 1, HelpText = "How many times to run each algorithm. Default, 1")]
        public int Count { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        // Global random generator
        private static Random rand = new Random();

        static void Main(string[] args)
        {
            Options opt = new Options();
            if (!CommandLine.Parser.Default.ParseArguments(args, opt))
            {
                Console.WriteLine(opt.GetUsage());
                return;
            }

            Console.WriteLine("Parsing Graph Definition");
            Graph G = Generator.CreateGraph(opt.InputFile);
            Console.WriteLine("Graph Delta={0}", G.Delta);

            Console.WriteLine("Setting enough threads");
            if (!ThreadPool.SetMinThreads(1000, 1))
            {
                Console.WriteLine("Too many threads...");
                return;
            }

            for (int i = 0; i < opt.Count; i++)
            {
                string[] algs = opt.Algorithm.Split(',');
                foreach (var _alg in algs)
                {
                    string alg = _alg.Trim();
                    if (string.Compare(alg, "Rand2Delta", true) == 0)
                    {
                        TestRand2D(G);
                    }
                    if (string.Compare(alg, "Rand2Delta2", true) == 0)
                    {
                        TestRand2D2(G);
                    }
                    if (string.Compare(alg, "RandDeltaPlus1", true) == 0)
                    {
                        TestRandDeltaPlus1(G);
                    }
                    if (string.Compare(alg, "FastRand", true) == 0)
                    {
                        TestFastRand(G, opt.Ro);
                    }
                    if (string.Compare(alg, "FastRand2", true) == 0)
                    {
                        TestFastRand2(G, opt.Ro);
                    }
                    if (string.Compare(alg, "FastRandDeltaPlus1", true) == 0)
                    {
                        TestFastRandDeltaPlus1(G, opt.Ro);
                    }
                }
            }
        }

        private static void TestGraphAlgorithm(Graph G, Func<ISynchronizer, Graph, int, Vertex> newVertex)
        {
            IList<Vertex> vertices = new List<Vertex>();
            ISynchronizer s = new SynchronizerNaive(vertices);

            EventWaitHandle doneEvent = new AutoResetEvent(false);
            s.Finished += (sender, args) => { doneEvent.Set(); };

            IDictionary<int, Vertex> tmp = new Dictionary<int, Vertex>();
            foreach (var v in G.Vertices)
            {
                Vertex vertex = newVertex(s, G, v);
                vertices.Add(vertex);

                tmp[v] = vertex;
            }

            foreach (var v in vertices)
            {
                foreach (var uId in G.getNeighbours(v.Id))
                {
                    v.addNeighbour(tmp[uId]);
                }
            }

            Console.WriteLine("Starting Algorithm!");
            s.Start();
            doneEvent.WaitOne();
        }

        private static void TestRand2D(Graph G)
        {
            TestGraphAlgorithm(G, (synch, graph, vId) => 
                                    RandODeltaVertex.newRand2DeltaVertex(synch, vId, G.Delta, rand));
        }

        private static void TestRand2D2(Graph G)
        {
            TestGraphAlgorithm(G, (synch, graph, vId) =>
                                    RandODeltaVertex.newRand2Delta2Vertex(synch, vId, G.Delta, rand));
        }

        private static void TestFastRand(Graph G, int ro)
        {
            TestGraphAlgorithm(G, (synch, graph, vId) =>
                                    FastRandColorVertex.newFastRandRand2DeltaVertex(synch, vId, G.Delta, G.TotalVertices, ro, rand));
        }

        private static void TestFastRand2(Graph G, int ro)
        {
            TestGraphAlgorithm(G, (synch, graph, vId) =>
                                    FastRandColorVertex.newFastRandRand2Delta2Vertex(synch, vId, G.Delta, G.TotalVertices, ro, rand));
        }

        private static void TestRandDeltaPlus1(Graph G)
        {
            TestGraphAlgorithm(G, (synch, graph, vId) =>
                                    RandODeltaVertex.newRandDeltaPlusOneVertex(synch, vId, G.Delta, rand));
        }

        private static void TestFastRandDeltaPlus1(Graph G, int ro)
        {
            TestGraphAlgorithm(G, (synch, graph, vId) =>
                                    FastRandColorVertex.newFastRandRandDeltaPlusOneVertex(synch, vId, G.Delta, G.TotalVertices, ro, rand));
        }

        private static void TestColeVishkin(string graphFilePath)
        {
            if (!ThreadPool.SetMinThreads(1000, 1))
            {
                Console.WriteLine("Too many threads...");
                return;
            }

            OrientedPath path = Generator.CreateGraphPath(graphFilePath);

            IList<Vertex> cvVertices = new List<Vertex>();
            ISynchronizer s = new SynchronizerNaive(cvVertices);

            IDictionary<int, ColeVishkinPathVertex> tmp = new Dictionary<int, ColeVishkinPathVertex>();
            foreach (var v in path.Vertices)
            {
                ColeVishkinPathVertex cvVertex = new ColeVishkinPathVertex(s, v, path.getColor(v));
                cvVertices.Add(cvVertex);

                tmp[v] = cvVertex;
            }

            foreach (ColeVishkinPathVertex cv in cvVertices)
            {
                var gamma_v = path.getNeighbours(cv.Id).GetEnumerator();
                gamma_v.MoveNext();

                cv.SetNeighbour1(tmp[gamma_v.Current]);
                if (gamma_v.MoveNext())
                    cv.SetNeighbour2(tmp[gamma_v.Current]);
            }

            s.Start();
            Console.ReadLine();
        }

        private void TestDummy()
        {
            int num;
            if (!ThreadPool.SetMinThreads(1000, 1)) return;

            while (true)
            {
                string x = Console.ReadLine();
                if (string.Compare(x, "q", true) == 0) break;

                IList<Vertex> dummyVerices = new List<Vertex>();
                ISynchronizer s = new SynchronizerNaive(dummyVerices);

                num = int.Parse(x);

                for (int j = 0; j < num; j++)
                {
                    DummyVertex v = new DummyVertex(s, j);
                    dummyVerices.Add(v);
                }
                Console.WriteLine("Done building objects");

                s.Start();
            }
        }
    }
}
