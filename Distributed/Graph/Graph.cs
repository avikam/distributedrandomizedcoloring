﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distributed.StaticGraph
{
    struct GraphVertex
    {
        public int Id;
        public List<GraphVertex> neighbours;
        public int Color;
    }

    public class Graph
    {
        private Dictionary<int, GraphVertex> graphDef;
        public int TotalVertices { get { return graphDef.Keys.Count; } }
        public int Delta { get; private set; }
        public int delta
        {
            get
            {
                if (_delta == -1)
                {
                    _delta = graphDef.Values.Select(v => v.neighbours.Count).Min();
                }
                return _delta;
            }
        }
        private int _delta;
        public Graph()
        {
            graphDef = new Dictionary<int, GraphVertex>();
            Delta = 0;
            _delta = 0;
        }

        public virtual void addVertex(int id)
        {
            GraphVertex v = new GraphVertex()
            {
                Id = id,
                neighbours = new List<GraphVertex>(),
                Color = -1
            };

            addVertex(v);
        }

        public virtual void addVertex(int id, int color)
        {
            GraphVertex v = new GraphVertex()
            {
                Id = id,
                neighbours = new List<GraphVertex>(),
                Color = color
            };

            addVertex(v);
        }

        public void addEdge(int uId, int vId)
        {
            _addEdge(uId, vId);
            _delta = -1; // invalidate delta
        }
        private void _addEdge(int uId, int vId)
        {
            GraphVertex u = graphDef[uId];
            GraphVertex v = graphDef[vId];
            if (u.neighbours.Contains(v))
                return;

            u.neighbours.Add(v);
            Delta = Math.Max(Delta, u.neighbours.Count);
        }

        public IEnumerable<int> Vertices
        {
            get { return graphDef.Keys; }
        }

        public IEnumerable<int> getNeighbours(int vId)
        {
            return graphDef[vId].neighbours.Select(u => u.Id);
        }

        public int getColor(int vId)
        {
            return graphDef[vId].Color;
        }

        private void addVertex(GraphVertex v)
        {
            if (graphDef.ContainsKey(v.Id))
                return;

            graphDef[v.Id] = v;
        }
    }
}
