﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Distributed.StaticGraph
{
    class Generator
    {
        enum ParsePhase
        {
            NoneInitialized,
            Vertex,
            Edge,
            Orientation
        }

        public static Graph CreateGraph(string filePath)
        {
            Graph G = new Graph();

            Regex regVertices = new Regex(@"\((\d+), (\d+)\)");
            Regex regEdges = new Regex(@"(\d+): ");
            Regex regOrient = new Regex(@"(\d+) -> (\d+)");

            ParsePhase pp = ParsePhase.NoneInitialized;

            StreamReader r = File.OpenText(filePath);
            string line;
            while ((line = r.ReadLine()) != null)
            {
                if (line.StartsWith("; vertices"))
                    pp = ParsePhase.Vertex;
                else if (line.StartsWith("; edges"))
                    pp = ParsePhase.Edge;
                else if (line.StartsWith("; orient"))
                    pp = ParsePhase.Orientation;
                else
                {
                    if (pp == ParsePhase.Vertex)
                    {
                        MatchCollection matches = regVertices.Matches(line);
                        if (matches.Count > 0)
                        {
                            Match m = matches[0];
                            //Console.WriteLine("new Vertex, id={0}, color={1}", int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                            G.addVertex(int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                        }
                    }
                    else if (pp == ParsePhase.Edge)
                    {
                        MatchCollection matches = regEdges.Matches(line);
                        if (matches.Count > 0)
                        {
                            Match m = matches[0];
                            //Console.Write("Edges from {0} - ", m.Groups[1].Value);

                            int u = int.Parse(m.Groups[1].Value);

                            Match n = Regex.Match(line.Substring(m.Length), @"\d+"); ;
                            while (n.Success)
                            {
                                //Console.Write("{0}; ", n.Value);
                                G.addEdge(u, int.Parse(n.Value));
                                n = n.NextMatch();
                            }

                            //Console.WriteLine("");
                        }
                    }
                    else
                    {
                        if (pp != ParsePhase.Orientation) throw new Exception(string.Format("Expected phase no to be {0}", pp.ToString()));

                        MatchCollection matches = regOrient.Matches(line);
                        if (matches.Count > 0)
                        {
                            Match m = matches[0];
                            //Console.WriteLine("Orientation {0} -> {1}", m.Groups[1].Value, m.Groups[2].Value);
                            //G.addOrientation(int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                            throw new NotImplementedException();
                        }
                        
                    }
                }

            }

            return G;
        }

        public static OrientedPath CreateGraphPath(string filePath)
        {
            OrientedPath G = new OrientedPath();

            Regex regVertices = new Regex(@"\((\d+), (\d+)\)");
            Regex regEdges = new Regex(@"(\d+): ");
            Regex regOrient = new Regex(@"(\d+) -> (\d+)");

            ParsePhase pp = ParsePhase.NoneInitialized;

            StreamReader r = File.OpenText(filePath);
            string line;
            while ((line = r.ReadLine()) != null)
            {
                if (line.StartsWith("; vertices"))
                    pp = ParsePhase.Vertex;
                else if (line.StartsWith("; edges"))
                    pp = ParsePhase.Edge;
                else if (line.StartsWith("; orient"))
                    pp = ParsePhase.Orientation;
                else
                {
                    if (pp == ParsePhase.Vertex)
                    {
                        MatchCollection matches = regVertices.Matches(line);
                        if (matches.Count > 0)
                        {
                            Match m = matches[0];
                            //Console.WriteLine("new Vertex, id={0}, color={1}", int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                            G.addVertex(int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                        }
                    }
                    else if (pp == ParsePhase.Edge)
                    {
                        MatchCollection matches = regEdges.Matches(line);
                        if (matches.Count > 0)
                        {
                            Match m = matches[0];
                            //Console.Write("Edges from {0} - ", m.Groups[1].Value);

                            int u = int.Parse(m.Groups[1].Value);

                            Match n = Regex.Match(line.Substring(m.Length), @"\d+"); ;
                            while (n.Success)
                            {
                                //Console.Write("{0}; ", n.Value);
                                G.addEdge(u, int.Parse(n.Value));
                                n = n.NextMatch();
                            }

                            //Console.WriteLine("");
                        }
                    }
                    else
                    {
                        if (pp != ParsePhase.Orientation) throw new Exception(string.Format("Expected phase no to be {0}", pp.ToString()));

                        MatchCollection matches = regOrient.Matches(line);
                        if (matches.Count > 0)
                        {
                            Match m = matches[0];
                            //Console.WriteLine("Orientation {0} -> {1}", m.Groups[1].Value, m.Groups[2].Value);
                            G.addOrientation(int.Parse(m.Groups[1].Value), int.Parse(m.Groups[2].Value));
                        }
                    }
                }

            }

            return G;
        }
    }
}
