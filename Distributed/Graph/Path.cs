﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distributed.StaticGraph
{

    class OrientedPath : Graph
    {
        private Dictionary<int, int> orientationDef;

        public OrientedPath() :
            base()
        {
            orientationDef = new Dictionary<int, int>();
        }

        public void addOrientation(int parent, int son) {
            if (orientationDef.ContainsKey(parent))
                throw new ArgumentException("parent already a son");

            orientationDef[parent] = son;
        }

        public int getSon(int parent)
        {
            if (!orientationDef.ContainsKey(parent))
                throw new ArgumentException("verex has no son");

            return orientationDef[parent];
        }
    }
}
