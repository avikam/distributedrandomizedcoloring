﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distributed
{
    class OrientingVertex : Vertex
    {

        private Vertex son;

        private Vertex u1;
        private Vertex u2;

        private object o;
        private int handledMessages;
        private AutoResetEvent allMessageArrivedEvent;


        public OrientingVertex(ISynchronizer synchronizer, int id)
            : base(synchronizer, id)
        {
            allMessageArrivedEvent = new AutoResetEvent(false);
            o = new object();

            handledMessages = 0;
        }

        public void SetNeighbour1(Vertex u)
        {
            this.u1 = u;
        }

        public void SetNeighbour2(Vertex u)
        {
            this.u2 = u;
        }

        public async override Task RunRound(uint round)
        {
            DebugState("====" + round.ToString() + "====");
            if (round == 0)
            {
                DebugState("RunRound#1");
                await RunRoundOrientPath();
                DebugState("RunRound#2");
            }
            else
            {
                Terminate();
            }
        }

        public override void OnMessage(object sender, Message args)
        {
            if (args.m.StartsWith("orient"))
            {
                lock (o)
                {
                    Interlocked.Increment(ref handledMessages);
                    DebugState(string.Format("Handling #msg={0}", handledMessages));


                    OnMemberIdMessage(args.v);

                    if (((u1 == null) || (u2 == null)) || handledMessages == 2)
                    {
                        allMessageArrivedEvent.Set();
                    }
                }
            }
        }

        private async Task RunRoundOrientPath()
        {
            DebugState("RunRoundOrPath");

            Task[] sendMessages = new Task[(u1 == null || u2 == null) ? 1 : 2];
            int j = 0;
            if (u1 != null) sendMessages[j++] = Send(u1, string.Format("orient u1 {0}", Id));
            if (u2 != null) sendMessages[j++] = Send(u2, string.Format("orient u2 {0}", Id));

            await Task.WhenAll(sendMessages).ConfigureAwait(false);
            //DebugState("OrPathBef");
            allMessageArrivedEvent.WaitOne();
            //DebugState("OrPathAfter");

            if (son == null)
                Console.WriteLine("{0} leaf", Id);
            else
                Console.WriteLine(string.Format("{0} -> {1}", Id, son.Id));
        }

        private void OnMemberIdMessage(Vertex neighbour)
        {
            DebugState(string.Format("Got nid={0}", neighbour.Id));

            if (neighbour.Id < Id)
                son = neighbour;
        }

    }
}
