﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distributed
{
    class FastRandColorVertex : RandODeltaVertex
    {
        private readonly double numFirstRounds;

        private ConcurrentDictionary<Vertex, int> parents;
        private ConcurrentDictionary<Vertex, int> sons;

        private CountdownEvent orientationCountdownEvent;
        private CountdownEvent property33CountdownEvent;
        private readonly IEnumerable<int> freshPallete;

        public static FastRandColorVertex newFastRandRand2DeltaVertex(ISynchronizer synchronizer, int id, int Delta, int N, int ro, Random rand)
        {
            return new FastRandColorVertex(synchronizer, id, Delta, N, ro, RandODeltaSettings.Rand2DeltaSettings(Delta), rand);
        }

        public static FastRandColorVertex newFastRandRand2Delta2Vertex(ISynchronizer synchronizer, int id, int Delta, int N, int ro, Random rand)
        {
            return new FastRandColorVertex(synchronizer, id, Delta, N, ro, RandODeltaSettings.Rand2Delta2Settings(Delta), rand);
        }

        public static FastRandColorVertex newFastRandRandDeltaPlusOneVertex(ISynchronizer synchronizer, int id, int Delta, int N, int ro, Random rand)
        {
            return new FastRandColorVertex(synchronizer, id, Delta, N, ro, RandODeltaSettings.RandDeltaPlus1Settings(Delta, rand), rand);
        }

        protected FastRandColorVertex(ISynchronizer synchronizer, int id, int Delta, int N, int ro, RandODeltaSettings randODeltaSettings, Random rand) 
            : base (synchronizer, id, Delta, randODeltaSettings, rand)
        {
            numFirstRounds = ro * Math.Ceiling(Math.Sqrt(Math.Log(N)));
            freshPallete = Enumerable.Range(randODeltaSettings.palleteSize + 1, randODeltaSettings.palleteSize + 1 + Delta);
            parents = new ConcurrentDictionary<Vertex, int>();
            sons = new ConcurrentDictionary<Vertex, int>();
        }

        public async override Task RunRound(uint round)
        {
            if (round == 0) // First: orient the vertices
            {
                orientationCountdownEvent = new CountdownEvent(neighbours.Count);
                Ready.Set();
                await OrientationRound();

                Ready.Reset();
            }
            else if (round < numFirstRounds + 1) // + 1 to compensate the orientation phase
            {
                await base.RunRound(round);
            }
            else // if active, draw using prop 3.3
            {
                property33CountdownEvent = new CountdownEvent(neighbours.Count - ColoredNeighbors);
                Ready.Set();
                bool res = await Propery33Round();
                Ready.Reset();
                if (res)
                {
                    Console.WriteLine("{0} colored using propery 3.3, in round {1}", Id, round);
                    Terminate();
                }
            }
        }

        private async Task OrientationRound()
        {
            await SendAllActiveNeighbours("orient:" + Id);

            // Wait untill all orientation messages consumed
            orientationCountdownEvent.Wait();
        }

        private async Task<bool> Propery33Round()
        {
            // Use \Delta+1 colors to complete the coloring using the acyclic orientation, Using fresh pallete.

            // All out degrees are \leq \Delta+1, and W.H.P, the largest length is of \Sqrt(log(N))

            // This vertex will only color it self, if all of its parents are colored. There must be one such
            // vertex since this orientation is acyclic and this argument can be argued in induction.
            if (parents.All(v => v.Value != -1))
            {
                // Select the first color from the fresh pallete that is not contained the parents' colors
                int newColor = freshPallete.First(c => !parents.Values.Contains(c));
                
                // Send all active neighbours that I am colored!
                await SendAllActiveNeighbours("property3.3:" + newColor);
                PrintColor(newColor);
                property33CountdownEvent.Wait();
                return true;
            }

            await SendAllActiveNeighbours("property3.3:no");
            property33CountdownEvent.Wait();
            return false;
        }

        public override void OnMessage(object sender, Message args)
        {
            bool isFinalMessage = args.m.StartsWith("final:");
            bool isProperty33Message = args.m.StartsWith("property3.3:");
            string color = args.m.Split(':')[1];
            bool msgHasColor = string.Compare(color, "no") != 0;

            if (isProperty33Message)
            {
                Ready.WaitOne();
                if (msgHasColor && parents.ContainsKey(args.v))
                {
                    parents[args.v] = int.Parse(color);
                    IncrementColoredNeighbours();
                }
                property33CountdownEvent.Signal();
            }
            else if (args.m.StartsWith("temp:") || isFinalMessage)
            {
                if (isFinalMessage && msgHasColor && parents.ContainsKey(args.v)) // keep track of used colors
                    parents[args.v] = int.Parse(color);

                base.OnMessage(sender, args);
            }
            else if (args.m.StartsWith("orient:"))
            {
                Ready.WaitOne();
                int uId = args.v.Id;
                if (uId > Id)
                {
                    parents.TryAdd(args.v, -1);
                }
                else
                {
                    sons.TryAdd(args.v, -1);
                }
                orientationCountdownEvent.Signal();
            }
        }
    }
}
