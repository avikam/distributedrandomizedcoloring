﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distributed
{
    class ColeVishkinPathVertex : Vertex
    {

        private Vertex son;
        
        private Vertex u1;
        private Vertex u2;


        private object o;

        private int handledMessages;
        private AutoResetEvent allMessageArrivedEvent;

        public int color { get; private set; }
        private int nextColor;

        public ColeVishkinPathVertex(ISynchronizer synchronizer, int id)
            : base(synchronizer, id)
        {
            color = id;
            allMessageArrivedEvent = new AutoResetEvent(false);
            o = new object();

            handledMessages = 0;
        }

        public ColeVishkinPathVertex(ISynchronizer synchronizer, int id, int color)
            : base(synchronizer, id)
        {
            this.color = color;
            allMessageArrivedEvent = new AutoResetEvent(false);
            o = new object();

            handledMessages = 0;
        }

        public void SetNeighbour1(Vertex u)
        {
            this.u1 = u;
        }

        public void SetNeighbour2(Vertex u)
        {
            this.u2 = u;
        }

        public void SetSon (Vertex u)
        {
            this.son = u;
        }
        
        /*
         * TODO: handle message may occure before round is started 
         */
        public async override Task RunRound(uint round)
        {
            DebugState("====" + round.ToString() + "====");
            if (round == 0)
            {
                 DebugState("RunRound#1");
                 await RunRoundOrientPath();
                 DebugState("RunRound#2");
            }
            //if (this.color >= 8)
            else if (round == 100)
            {
                Terminate();
            }
            else
            {
                await RunLowerColor();
                DebugState(string.Format("Color_{0}={1}", round, color));
            }
        }

        public override void OnMessage(object sender, Message args)
        {
            if (args.m.StartsWith("orient"))
            {
                lock (o)
                {
                    Interlocked.Increment(ref handledMessages);
                    DebugState(string.Format("Handling #msg={0}", handledMessages));


                    OnMemberIdMessage(args.v);

                    if (((u1 == null) || (u2 == null)) || handledMessages == 2)
                    {
                        allMessageArrivedEvent.Set();
                    }
                }
            }
            else if (args.m.StartsWith("pi_c"))
            {
                ColeVishkinPathVertex p = args.v as ColeVishkinPathVertex;
                DebugState(string.Format("handling color message={0}, ({1})", args.m, p.color));
                OnColorMessage(p.color);
                allMessageArrivedEvent.Set();
            }
        }

        private async Task RunLowerColor()
        {
            if (son != null)
            {
                await Send(son, string.Format("pi_c = {0}", color)).ConfigureAwait(false);
                if (u1 != null && u2 != null)
                    allMessageArrivedEvent.WaitOne();
                else // root: has a son but only one neighbour
                {
                    nextColor = color & 1; // picks the new color: <0, phi[0]>
                    color = nextColor;
                }
            }
            else // leaf: no need to send color
            {
                allMessageArrivedEvent.WaitOne();
            }

            color = nextColor;
        }

        private async Task RunRoundOrientPath()
        {
            DebugState("RunRoundOrPath");

            Task[] sendMessages = new Task[(u1 == null || u2 == null) ? 1 : 2];
            int j = 0;
            if (u1 != null) sendMessages[j++] = Send(u1, string.Format("orient u1 {0}", Id));
            if (u2 != null) sendMessages[j++] = Send(u2, string.Format("orient u2 {0}", Id));

            await Task.WhenAll(sendMessages).ConfigureAwait(false);
            //DebugState("OrPathBef");
            allMessageArrivedEvent.WaitOne();
            //DebugState("OrPathAfter");

            if (son == null)
                Console.WriteLine("{0} leaf", Id);
            else
                Console.WriteLine(string.Format("{0} -> {1}", Id, son.Id));
        }

        private void OnMemberIdMessage(Vertex neighbour)
        {
            DebugState(string.Format("Got nid={0}", neighbour.Id));

            if (neighbour.Id < Id)
                son = neighbour;
        }

        private void OnColorMessage(int parentColor)
        {
            // v must be son ??
            
            // v.color must be <> this.color

            int color_diff = color ^ parentColor;
            int curr_color = color; // for \phi[j]
            
            // get the position of the change
            int j = 0;
            while ((color_diff & 1) == 0)
            {
                j++;
                color_diff >>= 1;
                curr_color >>= 1;
            }

            // new color = <pos, c[pos]>
            this.nextColor = j << 1;
            nextColor |= (curr_color & 1);
        }
    }
}
