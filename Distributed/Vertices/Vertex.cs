﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Distributed
{
    public delegate void OnSendMessage(object sender, Message e);

    public class Message : EventArgs
    {
        public Message(Vertex v, string m)
        {
            this.v = v;
            this.m = m;
        }
        public Vertex v;
        public string m;
    }

    public class MessageQueue
    {
        //BlockingCollection<Message>
        public event OnSendMessage NewMessageEvent;

        public MessageQueue(Vertex q)
        {
            NewMessageEvent += new OnSendMessage(q.OnMessageEventHandle);
        }

        public void Send(Vertex src, string msg)
        {
            Message msgObj = new Message(src, msg);
            if (NewMessageEvent != null)
            {
                NewMessageEvent(src, msgObj);
            }
        }
    }

    public abstract class Vertex
    {
        protected IList<Vertex> neighbours;
        private MessageQueue messages;
        protected ISynchronizer synchronizer;

        public int Id { get; private set; }

        public bool Terminated { get; protected set; }
        
        // Signal when round is ready to process
        protected ManualResetEvent Ready;

        public Vertex(ISynchronizer synchronizer, int id)
        {
            Id = id;
            messages = new MessageQueue(this);
            neighbours = new List<Vertex>();
            Ready = new ManualResetEvent(false);
            Terminated = false;
            this.synchronizer = synchronizer;
        }

        public void addNeighbour(Vertex u)
        {
            if (!neighbours.Contains(u))
            {
                neighbours.Add(u);
            }
        }

        public virtual void OnMessageEventHandle(object sender, Message arg)
        {
            if (!Terminated)
                OnMessage(sender, arg);
        }

        public virtual void OnMessage(object sender, Message args)
        {
        }

        public abstract Task RunRound(uint round);

        public async void Run()
        {
            while (!Terminated && !synchronizer.Terminated)
            {
                // Start one round
                DebugState("Run #1");
                await RunRound(synchronizer.Cycle);
                DebugState("Run #2");

                // Notify round is ended
                //if (!synchronizer.Terminated)
                    synchronizer.EndRun();
            }

            // If vertex is terminated but the algorithm still running - just end my round
            while (!synchronizer.Terminated)
            {
                synchronizer.EndRun();
            }
            DebugState("Dead");
        }

        protected void Terminate()
        {
            if (!Terminated)
            {
                Terminated = true;
                synchronizer.Terminate(this);
                //synchronizer.EndRun();
            }
        }

        public void addMessage (Vertex src, string msg)
        {
            messages.Send(src, msg);
        }

        public Task Send(Vertex dst, string msg)
        {
            DebugState(string.Format("Sending {0}", msg));
            return Task.Run( () => 
                dst.addMessage(this, msg)
                );
            // Add message to Queue.
            
            // fire event
            // fire event
        }

        public Task Send(Vertex dst)
        {
            return Send(dst, "");
        }

        public Task SendAllActiveNeighbours(string msg)
        {
            if (neighbours != null)
            {
                IEnumerable<Task> sendAll = neighbours.Where(v => !v.Terminated).Select((v) => Send(v, msg));
                return Task.WhenAll(sendAll);
            }

            return Task.FromResult(0);
        }

        public void DebugState(string msg)
        {
            Console.WriteLine("id={1} thread={2} {0}", msg, Id.ToString().PadLeft(10), Thread.CurrentThread.ManagedThreadId);
        }
    }
}
