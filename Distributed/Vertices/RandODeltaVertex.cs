﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Distributed
{
    class RandODeltaVertex : Vertex
    {
        private Random rand;
        private CountdownEvent tmpColorArrived;
        private CountdownEvent finalColorArrived;
        private RandODeltaSettings settings;

        private List<int> ValidColors;
        private ConcurrentDictionary<int, bool> Tu;
        private ConcurrentDictionary<int, bool> Fu;
        private int coloredNeihbours;

        protected int ColoredNeighbors { get { return this.coloredNeihbours; } }

        public static RandODeltaVertex newRand2DeltaVertex(ISynchronizer synchronizer, int id, int Delta, Random rand)
        {
            RandODeltaSettings settings = RandODeltaSettings.Rand2DeltaSettings(Delta);
            return new RandODeltaVertex(synchronizer, id, Delta, settings, rand);
        }

        public static RandODeltaVertex newRandDeltaPlusOneVertex(ISynchronizer synchronizer, int id, int Delta, Random rand)
        {
            RandODeltaSettings settings = RandODeltaSettings.RandDeltaPlus1Settings(Delta, rand);
            return new RandODeltaVertex(synchronizer, id, Delta, settings, rand);
        }

        public static RandODeltaVertex newRand2Delta2Vertex(ISynchronizer synchronizer, int id, int Delta, Random rand)
        {
            RandODeltaSettings settings = RandODeltaSettings.Rand2Delta2Settings(Delta);
            return new RandODeltaVertex(synchronizer, id, Delta, settings, rand);
        }

        protected RandODeltaVertex(ISynchronizer synchronizer, int id, int Delta, RandODeltaSettings settings, Random rand)
            : base(synchronizer, id)
        {
            this.rand = rand;
            this.coloredNeihbours = 0;
            this.settings = settings;

            Fu = new ConcurrentDictionary<int, bool>();

            ValidColors = new List<int>();
            for (int i = 0; i < settings.palleteSize; i++)
            {
                ValidColors.Add(i);
            }
        }

        public async override Task RunRound(uint round)
        {
            Tu = new ConcurrentDictionary<int, bool>();

            if (settings.shouldCleanFinalColors)
                ValidColors.RemoveAll((c) => Fu.ContainsKey(c)); // need to do it before proccessing messages to avoid race
            tmpColorArrived = new CountdownEvent(neighbours.Count - this.coloredNeihbours);
            finalColorArrived = new CountdownEvent(neighbours.Count - this.coloredNeihbours);
            Ready.Set();

            bool colored = await RandRound();

            Tu = null;
            Ready.Reset();

            if (colored)
                Terminate();
        }

        private async Task<bool> RandRound()
        {
            bool shouldTose = settings.shouldTosePredicate();
            int c = -1;
            if (shouldTose)
            {
                int nextColorIndex = rand.Next(ValidColors.Count);
                c = ValidColors[nextColorIndex];
                await SendAllActiveNeighbours("temp:" + c);
            }
            else
            {
                await SendAllActiveNeighbours("temp:no");
            }

            tmpColorArrived.Wait(); // Wait untill T is fully assigned
            if (shouldTose && !Tu.ContainsKey(c) && !Fu.ContainsKey(c)) // if c /notin T /cup F
            {
                await SendAllActiveNeighbours("final:" + c);
                finalColorArrived.Wait(); // cleanup final messages

                PrintColor(c);
                return true;
            }

            await SendAllActiveNeighbours("final:no");
            finalColorArrived.Wait();
            return false;
        }

        public override void OnMessage(object sender, Message args)
        {
            Ready.WaitOne(); // This is to avoid handling messages before the vertex is ready to accept them

            if (args.m.StartsWith("temp:"))
            {
                string tmpColor = args.m.Split(':')[1];
                if (string.Compare(tmpColor, "no", StringComparison.OrdinalIgnoreCase) != 0)
                {
                    Tu.TryAdd(int.Parse(tmpColor), true);
                }
                tmpColorArrived.Signal();
            }
            else if (args.m.StartsWith("final:"))
            {
                string tmpColor = args.m.Split(':')[1];
                if (string.Compare(tmpColor, "no", StringComparison.OrdinalIgnoreCase) != 0)
                {
                    int finalNeighborColor = int.Parse(tmpColor);
                    Fu.TryAdd(finalNeighborColor, true);
                    IncrementColoredNeighbours();
                }

                finalColorArrived.Signal();
            }
        }

        protected void IncrementColoredNeighbours()
        {
            Interlocked.Increment(ref this.coloredNeihbours);
        }

        protected void PrintColor(int c)
        {
            Console.WriteLine("Vertex Color: id={0} c={1}", Id.ToString().PadLeft(10), c);
        }
    }
}
