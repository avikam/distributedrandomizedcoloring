﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distributed
{
    class DummyVertex : Vertex
    {
        public DummyVertex(ISynchronizer synch, int id) : base(synch, id) { }
        public override async Task RunRound(uint round)
        {
            if (round < 3)
            {
                Console.WriteLine(string.Format("Vertex {0}, Round {1}", this.Id, round));
            }
            else
            {
                Terminate();
            }
        }
    }
}
