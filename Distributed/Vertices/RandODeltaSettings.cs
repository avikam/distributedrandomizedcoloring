﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Distributed
{
    class RandODeltaSettings
    {
        public int palleteSize;
        public bool shouldCleanFinalColors;
        public Func<bool> shouldTosePredicate;

        public static RandODeltaSettings Rand2DeltaSettings(int Delta)
        {
            return new RandODeltaSettings
            {
                palleteSize = 2 * Delta,
                shouldCleanFinalColors = false,
                shouldTosePredicate = () => true
            };
        }

        public static RandODeltaSettings RandDeltaPlus1Settings(int Delta, Random rand)
        {
            return new RandODeltaSettings
            {
                palleteSize = Delta + 1,
                shouldCleanFinalColors = true,
                shouldTosePredicate = () => rand.Next(2) == 0
            };
        }

        public static RandODeltaSettings Rand2Delta2Settings(int Delta)
        {
            return new RandODeltaSettings
            {
                palleteSize = 2 * Delta,
                shouldCleanFinalColors = true,
                shouldTosePredicate = () => true
            };
        }
    }
}
