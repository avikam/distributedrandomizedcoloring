import random
import sys

N = int(sys.argv[1])

vertices = range(1, N+1)
colors = range(1, N+1)
random.shuffle(colors)

print "; vertices ids, colors"
for v, color in zip(vertices, colors):
    print "(%d, %d)" % (v, color)

print "; edges"
print "1: 2"
print "%d: %d" % (N, N-1)
for v in range(2, N):
    print "%d: %d, %d" % (v, v+1, v-1)
    
    
print "; orient"
# no need
