import networkx
from optparse import OptionParser

def gen_graph(options):
    if options.complete != -1:
        return networkx.complete_graph(options.complete)
    if options.wheel != -1:
        return networkx.wheel_graph(options.wheel)
    if options.star != -1:
        return networkx.star_graph(options.star)
    if options.cycle!= -1:
        return networkx.cycle_graph(options.cycle)
    if options.path!= -1:
        return networkx.path_graph(options.path)
    
        
def dump_graph(G, fname):
    with file(fname, "w") as f:
        f.write("; vertices ids, colors\n")
        for v in G.nodes():
            f.write("(%d, %d)\n" % (v, 0))
        
        f.write("; edges\n")
        for v in G.nodes():
            neighbors = ", ".join([str(x) for x in G.neighbors(v)])
            f.write("%d: %s\n" % (v, neighbors))
    
def main():
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename",
                      help="write report to FILE", metavar="FILE")
    parser.add_option("-c", "--complete",
                      type="int", dest="complete", default=-1,
                      help="complete graph")
    parser.add_option("-w", "--wheel",
                      type="int", dest="wheel", default=-1,
                      help="wheel graph")
    parser.add_option("-s", "--star",
                      type="int", dest="star", default=-1,
                      help="star graph")
    parser.add_option( "--cycle",
                      type="int", dest="cycle", default=-1,
                      help="star cycle")

    parser.add_option("-p", "--path",
                      type="int", dest="path", default=-1,
                      help="path graph")

    (options, args) = parser.parse_args()
    
    G = gen_graph(options)
    dump_graph(G, options.filename)
    
if __name__ == "__main__":
    main()