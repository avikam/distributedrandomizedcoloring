##########################################
## Automating running of the algorithm  ##
##########################################

import subprocess
from datetime import datetime
from functools import partial
from itertools import product
import re
import networkx
import mkgraph

Rand2Delta = "Rand2Delta"
RandDeltaPlus1 = "RandDeltaPlus1"
Rand2Delta2 = "Rand2Delta2"

FastRand = "FastRand"
FastRandDeltaPlus1 = "FastRandDeltaPlus1"
FastRand2 = "FastRand2"

class RunResult:
    def __init__(self, test):
        self.test = test
        self._result = []
    
    def addResult(self, cycles, colors, maxColor):
        self._result.append({"cycles": cycles,
                            "colors": colors,
                            "maxColor": maxColor})
    
    def graphName(self):
        return self.test.graph.name
    def graphNodes(self):
        return self.test.graph.number_of_nodes()
    def times(self):
        return self.test.times
    def alg(self):
        return self.test.alg

    def avgCycles(self):
        return sum([r["cycles"] for r in self._result]) / float(self.test.times)

class TestDefinition:
    def __init__(self, graph, times, *algs, **kwargs):
        self.graph = graph                  # networkx graph
        self.times = times                  # how many times to run the rest
        self.alg = ",".join(algs)
        self.ro = kwargs.get("ro", 1)
        
        self.result = RunResult(self)
        
        self.color_pattern = re.compile("^Vertex Color: id=(.*?)c=(.*?)$")
        self.end_pattern = re.compile(r"^All vertexes done, within (\d+) cycles$")
        
        nowtime = datetime.now().strftime("%Y%m%d-%H%M%S")
        self.settings = {
            "graph": "test-%s-%s-%s.graph" % (nowtime, self.graph.name, self.alg),
            "mkerr": lambda(i): "test-%s-%s-%s.err" % (nowtime, self.alg, i)
        }
    
    def run(self):
        # Create graph temp file
        mkgraph.dump_graph(self.graph, self.settings["graph"])
        
        print "Testing %s, ro=%s on %s" % (self.alg, self.ro, self.graph.name)  
        for i in range(self.times):
            try:
                g = self.graph.copy()
            
                output = self._run_command()
                total_rounds, colors = self._color_graph(output.split("\r\n"), g)
                assert self._verify(g), "Test wasn't successful"
            
                print "Run %d Ok!, rounds = %d, #colors=%d, max color=%d" % (i, total_rounds, len(colors), max(colors))
                self.result.addResult(total_rounds, len(colors), max(colors))
            except Exception as e:
                print e
                with file(self.settings["mkerr"](i), "wb") as f:
                    f.write(str(e) + "\r\n")
                    f.write(output)
                    
        self._print_summary()
        return self.result
    
    def _run_command(self):
        buff = ""
        p = subprocess.Popen(r"Distributed\bin\Debug\Distributed.exe -a %s -r %s -i %s" % 
                    (self.alg, self.ro, self.settings["graph"]),stdout=subprocess.PIPE)
        for line in iter(p.stdout.readline,''):
            buff += line
            if line.startswith("All vertexes done"):
                p.kill()
                return buff
        #return subprocess.check_output(r"Distributed\bin\Debug\Distributed.exe -a %s -r %s -i %s" % 
        #            (self.alg, self.ro, self.settings["graph"]))
    
    def _color_graph(self, output, G):
        colors = set()
        for l in output:
            m = self.color_pattern.match(l)
            if m is not None:
                id, c = [n.strip() for n in m.groups()]
                if "color" in G.node[int(id)]:
                    raise Exception("Same node colored twice!: %s: old=%s, new=%s" % (id, G[id]["color"], c))
                G.node[int(id)]["color"] = c
                colors.add(int(c))
            else:
                m = self.end_pattern.match(l)
                if m is not None:
                    total_rounds = int(m.groups(0)[0])
        
        return total_rounds, colors
    
    def _verify(self, G):
        for e in G.edges():
            v,u = [G.node[i] for i in e]
            if v["color"] == u["color"]:
                return False
                
        return True
    
    def _print_summary(self):
        print "all ok"


#g1 = networkx.gnp_random_graph (300, 0.3)
#g2 = networkx.gnp_random_graph (500, 0.1)
# tests = [
    # (partial (networkx.complete, 3), Rand2Delta, 100),
    # (partial (networkx.complete_graph, 30), Rand2Delta, 100),
    # (lambda: g1, Rand2Delta, 10),
    # (lambda: g1, FastRand, 10),
    # (lambda: g2, Rand2Delta, 10),
    # (lambda: g2, FastRand, 10),
    # (partial (networkx.cycle_graph, 700), Rand2Delta, 10),
    # (partial (networkx.cycle_graph, 700), RandDeltaPlus1, 10),
    # (partial (networkx.cycle_graph, 700), FastRand, 10, {"ro": 2}),
    # (partial (networkx.cycle_graph, 700), FastRandDeltaPlus1, 10, {"ro": 2}),
    
    # (partial (networkx.cycle_graph, 700), FastRandDeltaPlus1, 10, {"ro": 2}),
    # ]
    
def TestRunner(graph, alg, times, **kwargs):
    test = TestDefinition(graph, times, alg, **kwargs)
    return test.run()
    
def testsBatchRunner(testDefinitions, summaryFunc):
    for t in testDefinitions:
        cgraph, alg, time = t[:3]
        G = cgraph()
        if len(t) > 3:
            result = TestRunner(G, alg, time, **t[3])
        else:
            result = TestRunner(G, alg, time)
        
        yield summaryFunc(G, result)

def testWithSimpleSummary(testDefinitions, outfile):
    r = testsBatchRunner(
                testDefinitions,
                lambda G,result:
                    "%s,%s,%s,%s,%s\n" %   (result.graphName(), result.graphNodes(), 
                                            result.alg(), result.times(),
                                            result.avgCycles()))
    file(outfile, "w").writelines(r)


# for alg in [Rand2Delta, Rand2Delta2, RandDeltaPlus1, FastRand, FastRand2, FastRandDeltaPlus1]:
    # testWithSimpleSummary(
        # [(partial (networkx.cycle_graph, x), alg, 20) for x in xrange(10, 950, 5)],
        # "cycle-%s-result.csv" % alg)

    # testWithSimpleSummary(
        # [(partial (networkx.wheel_graph, x), alg, 20) for x in xrange(10, 950, 5)],
        # "wheel-%s-result.csv" % alg)

    # testWithSimpleSummary(
        # [(partial (networkx.complete_graph, x), alg, 20) for x in xrange(10, 700, 5)],
        # "complete-%s-result.csv" % alg)
    
    # testWithSimpleSummary(
        # [(partial (networkx.star_graph, x), alg, 20) for x in xrange(10, 700, 5)],
        # "star-%s-result.csv" % alg)
    
    # testWithSimpleSummary(
        # [(partial (networkx.path_graph, x), alg, 20) for x in xrange(10, 700, 5)],
        # "path-%s-result.csv" % alg)


for alg in [Rand2Delta, Rand2Delta2, RandDeltaPlus1, FastRand, FastRand2, FastRandDeltaPlus1]:
    # generator for the graphs
    graphs = (networkx.gnp_random_graph(n, p/10.0) for n,p in product(range(100, 950, 50), range(1, 9, 2)))
    testWithSimpleSummary(
            [(lambda: g, alg, 20) for g in graphs],
            "rand-%s-result.csv" % alg)
